const express = require("express");
const app = express();

app.use(express.urlencoded({extended:true}));
app.use(express.static("Public"))
app.use(express.json())

app.get("/", (req,res)=>{
    res.sendFile(__dirname+"/index.html");
})

app.post("/result", (req,res)=>{
    const inputNum = req.body.digits
    var randomNum = [];
    for(var i=0;i<inputNum;i++)
    {
      randomNum.push( Math.floor(Math.random()*10))
    }

    res.send("<h2>Generated Random Numbers</h2>"+randomNum);
    
})


app.listen(3001, ()=>{
    console.log("Server running at Port 3001");
});
